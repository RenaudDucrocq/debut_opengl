#include <stdexcept>
#include <iostream>
#include "CGameEngine.hpp"

int main(int argc, char **argv) {
    Ge::SWindowCreateInfo windowCreateInfo{};
    windowCreateInfo.window_width = 1280;
    windowCreateInfo.window_height = 720;
    windowCreateInfo.p_window_title = "Tutoriel_03";

    Ge::SRendererCreateInfo rendererCreateInfo{};
    rendererCreateInfo.e_renderer_type = Ge::ERendererType::OpenGL33;
    rendererCreateInfo.p_window_create_info = &windowCreateInfo;

    Ge::SRenderingEngineCreateInfo renderingEngineCreateInfo{};
    renderingEngineCreateInfo.eRenderingEngineType = Ge::ERenderingEngineType::Rasterization;
    renderingEngineCreateInfo.p_renderer_create_info = &rendererCreateInfo;

    Ge::SGameEngineCreateInfo gameEngineCreateInfo{};
    gameEngineCreateInfo.p_rendering_engine_create_info = &renderingEngineCreateInfo;

    Ge::CGameEngine gameEngine;

    try {
        gameEngine.Initialise(gameEngineCreateInfo);
        gameEngine.Run();
    } catch (std::runtime_error &error) {
        std::cerr << "Exception : " << error.what() << std::endl;
    } catch (std::bad_alloc &error) {
        std::cerr << "Exception : " << error.what() << std::endl;
    }

    return 0;
}
