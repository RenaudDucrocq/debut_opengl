//
// Created by renau on 08/04/2022.
//

#include "Rendering/Renderer/IWindow.hpp"

namespace Ge {
    GLFWwindow *IWindow::GetHandle() {
        return mp_window;
    }
}
