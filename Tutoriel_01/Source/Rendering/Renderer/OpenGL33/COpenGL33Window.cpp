//
// Created by renau on 08/04/2022.
//
#include "Rendering/Renderer/OpenGL33/COpenGL33Window.hpp"
#include <stdexcept>
#include <iostream>

namespace Ge {
    void errorCallback(int error, const char *description) {
        std::cerr << "error :" << description << std::endl;
    }

    COpengGL33Window::~COpengGL33Window() {
        Release();
    }

    void COpengGL33Window::Initialise(const SWindowCreateInfo &window_create_info) {
        if (m_initialised) Release();

        if (!glfwInit()) {
            throw std::runtime_error("failed to init GLFW");
        }
        m_initialised = true;
        glfwSetErrorCallback(errorCallback);
        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        mp_window = glfwCreateWindow(window_create_info.window_width,
                                     window_create_info.window_height,
                                     window_create_info.p_window_title,
                                     nullptr, nullptr);
        if (!mp_window) {
            glfwTerminate();
            throw std::runtime_error("failed to create window");
        }
        glfwMakeContextCurrent(mp_window);

        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            glfwTerminate();
            throw std::runtime_error("failed to init glew");
        }
        glfwSetInputMode(mp_window, GLFW_STICKY_KEYS, GL_TRUE);
        std::cout << "OpenGL 3.3 window init" << std::endl;
    }

    void COpengGL33Window::Release() {
        if (m_initialised) {
            glfwTerminate();
            mp_window = nullptr;
            m_initialised = false;
            std::cout << "OpenGL 3.3 window released" << std::endl;

        }
    }

}
