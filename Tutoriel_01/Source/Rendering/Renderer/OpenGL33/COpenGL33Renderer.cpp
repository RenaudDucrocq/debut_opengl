//
// Created by renau on 08/04/2022.
//
#include <iostream>
#include "Rendering/Renderer/OpenGL33/COpenGL33Renderer.hpp"

namespace Ge {

    COpenGL33Renderer::~COpenGL33Renderer() {
        Release();
    }

    void COpenGL33Renderer::Initialise(const SRendererCreateInfo rendererCreateInfo) {
        if (m_initialised) {
            Release();
        }
        m_initialised = true;
        m_window.Initialise(*rendererCreateInfo.p_window_create_info);
        mp_window = &m_window;

        std::cout << "Open GL 3.3 renderer init" << std::endl;
    }

    void COpenGL33Renderer::Release() {
        if (m_initialised) {
            m_window.Release();
            mp_window = nullptr;
            m_initialised = false;
            std::cout << "OpenGL 3.3 renderer released" << std::endl;
        }
    }
}