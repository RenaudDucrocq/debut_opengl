//
// Created by renau on 09/04/2022.
//
#include <stdexcept>
#include <iostream>
#include "Rendering/Rasterization/CRasterizationEngine.hpp"
#include "Rendering/Renderer/OpenGL33/COpenGL33Renderer.hpp"

namespace Ge {

    CRasterizationEngine::~CRasterizationEngine() {
        Release();
    }

    void CRasterizationEngine::Initialise(const SRenderingEngineCreateInfo renderingEngineCreateInfo) {
        if (m_initialised) Release();

        switch (renderingEngineCreateInfo.p_renderer_create_info->e_renderer_type) {
            case OpenGL33:
                mp_renderer = new COpenGL33Renderer();
                break;
            case OpenGL45:
                throw std::runtime_error("OpenGL 4.5 renderer not implemented");
            case Vulkan:
                throw std::runtime_error("Vulkan renderer not implemented");;
        }

        if (!mp_renderer) throw std::bad_alloc();
        m_initialised = true;
        mp_renderer->Initialise(*renderingEngineCreateInfo.p_renderer_create_info);

        std::cout << "Rasterization engine init " << std::endl;
    }

    void CRasterizationEngine::Release() {
        if (m_initialised) {
            if (mp_renderer) {
                mp_renderer->Release();
                delete mp_renderer;
                mp_renderer = nullptr;
            }
            m_initialised = false;
            std::cout << "Rasterization engine released" << std::endl;
        }
    }

    void CRasterizationEngine::Render(float lag) {
        glfwSwapBuffers(mp_renderer->GetWindow()->GetHandle());
    }
}
