//
// Created by renau on 09/04/2022.
//
#include <iostream>
#include <stdexcept>
#include "CGameEngine.hpp"
#include "Rendering/Rasterization/CRasterizationEngine.hpp"

namespace Ge {

    CGameEngine::~CGameEngine() {
        Release();
    }

    void CGameEngine::Initialise(const SGameEngineCreateInfo &gameEngineCreateInfo) {
        if (m_initialised) Release();

        switch (gameEngineCreateInfo.p_rendering_engine_create_info->eRenderingEngineType) {

            case Rasterization:
                mp_rendering_engine = new CRasterizationEngine();
                break;
            case RayTracingCPU:
                throw std::runtime_error("CPU Ray tracing engine not implemented");
            case RayTracingGPU:
                throw std::runtime_error("GPU Ray tracing engine not implemented");
        }

        if (!mp_rendering_engine)
            throw std::bad_alloc();
        m_initialised = true;
        mp_rendering_engine->Initialise(*gameEngineCreateInfo.p_rendering_engine_create_info);

        std::cout << "Game engine init" << std::endl;
    }

    void CGameEngine::Release() {
        if (m_initialised) {
            if (mp_rendering_engine) {
                mp_rendering_engine->Release();
                delete mp_rendering_engine;
                mp_rendering_engine = nullptr;
            }

            std::cout << "Game engine released" << std::endl;
        }
    }

    void CGameEngine::Run() {
        if (!m_initialised)throw std::runtime_error("Game engine not init");

        GLFWwindow *p_window = mp_rendering_engine->GetRenderer()->GetWindow()->GetHandle();
        //Timer
        double lag = 0.0;
        double previous = glfwGetTime();
        double SECONDS_PER_UPDATE = 1.0 / 60.0;
        while (!glfwWindowShouldClose(p_window) &&
               glfwGetKey(p_window, GLFW_KEY_ESCAPE) != GLFW_PRESS) {
            double current = glfwGetTime();
            double elapsed = current - previous;
            previous = current;

            lag += elapsed;
            glfwPollEvents();

            while (lag >= SECONDS_PER_UPDATE) {
                lag -= SECONDS_PER_UPDATE;
            }
            mp_rendering_engine->Render(static_cast<float>((float) lag / SECONDS_PER_UPDATE));
        }
    }
}
