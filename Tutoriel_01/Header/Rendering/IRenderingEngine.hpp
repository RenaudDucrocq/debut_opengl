//
// Created by renau on 09/04/2022.
//

#ifndef GAMEENGINE_IRENDERINGENGINE_HPP
#define GAMEENGINE_IRENDERINGENGINE_HPP

#include "Rendering/Renderer/IRenderer.hpp"

namespace Ge {
    enum ERenderingEngineType {
        Rasterization, RayTracingCPU, RayTracingGPU
    };
    struct SRenderingEngineCreateInfo {
        ERenderingEngineType eRenderingEngineType;
        SRendererCreateInfo *p_renderer_create_info;
    };

    class IRenderingEngine {
    public:
        virtual ~IRenderingEngine() = default;

        virtual void Initialise(const SRenderingEngineCreateInfo renderingEngineCreateInfo) = 0;

        virtual void Release() = 0;

        virtual void Render(float lag) = 0;

        IRenderer *GetRenderer();

    protected:
        bool m_initialised = false;
        IRenderer *mp_renderer = nullptr;
    };
}
#endif //GAMEENGINE_IRENDERINGENGINE_HPP
