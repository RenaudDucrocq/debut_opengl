//
// Created by renau on 09/04/2022.
//

#ifndef GAMEENGINE_CRASTERIZATIONENGINE_HPP
#define GAMEENGINE_CRASTERIZATIONENGINE_HPP
#include "Rendering/IRenderingEngine.hpp"
namespace Ge{
    class CRasterizationEngine: public IRenderingEngine{
    public:
        ~CRasterizationEngine() override;

        void Initialise(const SRenderingEngineCreateInfo renderingEngineCreateInfo) final;

        void Release() final;

        void Render(float lag) final;
    protected:

    };
}
#endif //GAMEENGINE_CRASTERIZATIONENGINE_HPP
