//
// Created by renau on 08/04/2022.
//

#ifndef GAMEENGINE_C_OPEN_GL_33_WINDOW_HPP
#define GAMEENGINE_C_OPEN_GL_33_WINDOW_HPP

#include "Rendering/Renderer/IWindow.hpp"

namespace Ge {
    class COpengGL33Window : public IWindow {
    public:
        ~COpengGL33Window() override;

        void Initialise(const SWindowCreateInfo &window_create_info) final;

        void Release() final;
    };
}

#endif //GAMEENGINE_C_OPEN_GL_33_WINDOW_HPP
