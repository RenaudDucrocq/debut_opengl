//
// Created by renau on 08/04/2022.
//

#ifndef GAMEENGINE_COPENGL33RENDERER_HPP
#define GAMEENGINE_COPENGL33RENDERER_HPP

#include "Rendering/Renderer/IRenderer.hpp"
#include "Rendering/Renderer/OpenGL33/COpenGL33Window.hpp"
namespace Ge {
    class COpenGL33Renderer : public IRenderer {
    public:
        ~COpenGL33Renderer() override;

        void Initialise(const SRendererCreateInfo rendererCreateInfo) final;

        void Release() final;

    private:
        COpengGL33Window m_window;
    };
}
#endif //GAMEENGINE_COPENGL33RENDERER_HPP
