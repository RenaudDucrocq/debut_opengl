//
// Created by renau on 08/04/2022.
//

#ifndef GAMEENGINE_I_WINDOW_HPP
#define GAMEENGINE_I_WINDOW_HPP

#include "Glew/glew.h"
#include "Glfw/glfw3.h"


namespace Ge {
    struct SWindowCreateInfo {
        int window_height;
        int window_width;
        const char *p_window_title;
    };

    class IWindow {
    public:
        virtual ~IWindow() = default;

        virtual void Initialise(const SWindowCreateInfo &window_create_info) = 0;

        virtual void Release() = 0;

        GLFWwindow *GetHandle();

    protected:
        bool m_initialised = false;
        GLFWwindow *mp_window = nullptr;
    };
}

#endif //GAMEENGINE_I_WINDOW_HPP
