//
// Created by renau on 08/04/2022.
//


#ifndef GAMEENGINE_IRENDERER_HPP
#define GAMEENGINE_IRENDERER_HPP

#include "Rendering/Renderer/IWindow.hpp"

namespace Ge {
    enum ERendererType {
        OpenGL33,
        OpenGL45,
        Vulkan
    };
    struct SRendererCreateInfo {
        ERendererType e_renderer_type;
        SWindowCreateInfo *p_window_create_info;
    };

    class IRenderer {
    public:
        virtual ~IRenderer() = default;

        virtual void Initialise(const SRendererCreateInfo rendererCreateInfo) = 0;

        virtual void Release() = 0;

        IWindow *GetWindow();

    protected:
        bool m_initialised = false;
        IWindow *mp_window = nullptr;
    };
}
#endif //GAMEENGINE_IRENDERER_HPP
