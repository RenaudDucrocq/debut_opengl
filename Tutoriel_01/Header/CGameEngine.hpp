//
// Created by renau on 09/04/2022.
//

#ifndef GAMEENGINE_CGAMEENGINE_HPP
#define GAMEENGINE_CGAMEENGINE_HPP

#include "Rendering/IRenderingEngine.hpp"

namespace Ge {
    struct SGameEngineCreateInfo {
        SRenderingEngineCreateInfo *p_rendering_engine_create_info;
    };

    class CGameEngine {
    public:
        ~CGameEngine();

        void Initialise(const SGameEngineCreateInfo &gameEngineCreateInfo);

        void Release();

        void Run();

    private:
        bool m_initialised = false;
        IRenderingEngine *mp_rendering_engine = nullptr;
    };
}
#endif //GAMEENGINE_CGAMEENGINE_HPP
